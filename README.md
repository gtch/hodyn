# Hodyn

Integrate your home Dynalite system into the new world of smart home technologies.

## What is this?

Hodyn is for people who have a Philips Dynalite lighting system in their home, and want to connect it to
other smart home technologies like Apple HomeKit, Google Home, openHAB or any other platform. Hodyn is
still very new and so doesn't actually support any of those integrations... yet. 

Hodyn was created by me, Charles Gutjahr, primarily for my own use. Over time 
I expect it will become useful to anyone who has Dynalite installed in their home, but right now it's probably 
only useful for me.


### Features

* Displays temperature and lux levels for Antumbra panels
* Displays status of all lights (zones)
* Can turn lights on and off, and to any preset level 
* Can control window louvres

## How to use Hodyn

### Requirements

* A Dynalite system
* A Dynalite EnvisionGateway connected to your home network

### Starting up Hodyn

Hodyn is currently released only in source code form, and so you'll need some basic knowledge 
of software development to be able to use it. 

To start up Hodyn:

  * Install Elixir
  * Install Node.js
  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### Configuration

Hodyn needs information about the setup of your Dynalite system to be able to function. 

Provide this in a file called `setup.json` in the root folder of Hodyn. The file must contain
JSON structured like this example:

    {
      "gateway": {
        "host": "10.1.1.1",
        "port": 50000
      },
      "areas": [
        { "area": 1, "name": "Kitchen" },
        { "area": 2, "name": "Bedroom" }
      ],
      "boxes": [
        { "box": 11, "name": "Kitchen (Left)" },
        { "box": 12, "name": "Kitchen (Right)" }
      ],
      "louvres": [
        { "name": "Kitchen Window", "trigger": 110, "relay": 111 }
      ]
    }

* The IP address and TCP port of your Dynalite EnvisionGateway are specified in `gateway`
* The names and area numbers of each lighting area are specified in `areas`
* The names and box numbers of each Antumbra panel are specified in `boxes`
* The name, trigger and relay zones of each window louvre are specified in `louvres`
  * The trigger is the zone which causes a window louvre to open or close when queried
  * The relay is the zone which actually opens or closes the window


## Copyright and contributions

Hodyn is copyright but open-source. That means you may download it and use it free of charge, as long
as you comply with the licence rules. 

I'm not expecting contributions, but they would be welcome. If you contribute you represent that you have 
the right to license your contribution to me, Charles Gutjahr, and agree that your contributions are licensed 
to others under the Apache License 2.0.

-------------------------------------------------------------------------------------------------------

Copyright 2017 Charles Gutjahr <[gtch@hodyn.net](mailto:gtch@hodyn.net)>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.