defmodule Hodyn.PageController do
  use Hodyn.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def lights(conn, _params) do
    render conn, "lights.html"
  end

  def louvres(conn, _params) do
    render conn, "louvres.html"
  end

  def panels(conn, _params) do
    render conn, "panels.html"
  end
end
