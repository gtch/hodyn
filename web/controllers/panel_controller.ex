defmodule Hodyn.PanelController do
  use Hodyn.Web, :controller

  def show(conn, %{"id" => id}) do
    panel = %{ box: id, lux: 400, temperature: 24.5 }
    render(conn, "show.json", panel: panel)
  end

end
