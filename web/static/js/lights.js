import socket from "./socket"

let lightsTable = $(".lights-areas");

if (lightsTable.length > 0) {
  // This page has a lights table, so connect to the lights channel and setup lighting functionality
  let channel = socket.channel("dynalite:light", {});

  let presetNames = {
    1: "High",
    2: "Medium",
    3: "Low",
    4: "Off",
    5: "Night"
  };


  let updateTime = function(time) {
    $(".lights-lastupdated").text("Last updated " + time);
  };

  let setAreaToPreset = function(event) {
    let target = $(event.target);
    let area = target.data("area");
    let preset = target.data("preset") + 0;
    console.log("setAreaToPreset", area, preset);
    if (preset >= 1 && preset <= 4) {
      // A valid preset was chosen
      channel.push("area_preset", { area: area, preset: preset });
    } else {
      log.warn("Invalid preset " + target.data("preset") + " for area " + area);
    }

  };

  channel.on("list", payload => {
    lightsTable.empty();
    payload.areas.forEach(areaObject => {
      console.log("area", areaObject);
      let name = areaObject.name;
      let area = areaObject.area;
      let areaTd = $("<td>").html(name + " <span class=\"idnum\">(" + area + ")</span>");
      let statusTd = $("<td class='light-status'>").data("area", area).addClass("light-status-" + area);
      let highButton = $("<button class='area-high'>High</button>").data("area", area).data("preset", 1).click(setAreaToPreset);
      let mediumButton = $("<button class='area-medium'>Medium</button>").data("area", area).data("preset", 2).click(setAreaToPreset);
      let lowButton = $("<button class='area-low'>Low</button>").data("area", area).data("preset", 3).click(setAreaToPreset);
      let offButton = $("<button class='area-off'>Off</button>").data("area", area).data("preset", 4).click(setAreaToPreset);
      let actionTd = $("<td class='light-actions'>").data("area", area).addClass("light-actions-" + area).append(highButton).append(mediumButton).append(lowButton).append(offButton);
      lightsTable.append($("<tr>").append(areaTd, statusTd, actionTd));
    });
    channel.push("update_all", {});
  });


  channel.on("areastatus", payload => {
    console.log("payload areastatus", payload);
    let area = payload.area;
    let preset = payload.preset;
    let statusCell = $(".light-status-" + area);
    if (presetNames[preset]) {
      statusCell.text(presetNames[preset]);
    } else {
      statusCell.text("Unknown (" + present + ")");
    }

    updateTime(payload.time);
  });



  document.querySelector(".lights-list").addEventListener("click", event => {
    channel.push("update_all", {})
  });



  channel.join()
    .receive("ok", resp => {
      // If we successfully join the channel, update the list of lights
      console.log("Joined successfully", resp);
      channel.push("list", {});
    })
    .receive("error", resp => {
      console.log("Unable to join", resp);
    })


}
