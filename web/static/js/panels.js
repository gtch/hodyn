import socket from "./socket"

let panelsTable = $(".panels-boxes");

if (panelsTable.length > 0) {
  // This page has a panels table, so connect to the panels channel and setup panel functionality
  let channel = socket.channel("dynalite:panel", {});

  let updateTime = function (time) {
    $(".panels-lastupdated").text("Last updated " + time);
  };

  channel.on("list", payload => {
    panelsTable.empty();
    payload.boxes.forEach(boxObject => {
      console.log("box", boxObject);
      let name = boxObject.name;
      let box = boxObject.box;
      let boxTd = $("<td>").html(name + " <span class=\"idnum\">(" + box + ")</span>");
      let tempTd = $("<td class='panel-temperature'>").data("box", box).addClass("panel-box-" + box);
      let luxTd = $("<td class='panel-lux'>").data("box", box).addClass("panel-lux-" + box);
      let identifyButton = $("<button class='panel-identify'>Identify</button>");
      let actionTd = $("<td>").append(identifyButton);
      identifyButton.click(item => {
        console.log("Identifying box " + box);
        channel.push("identify", {"box": box})
      })
      panelsTable.append($("<tr>").append(boxTd, tempTd, luxTd));
    })
    channel.push("update_all", {});
  })

  channel.on("temperature", payload => {
    console.log("payload temperature", payload);
    let box = payload.box;
    let temperature = payload.temperature;
    let temperatureCell = $(".panel-box-" + box);
    temperatureCell.text(temperature + "°");
    updateTime(payload.time);
  })

  channel.on("lux", payload => {
    console.log("payload lux", payload);
    let box = payload.box;
    let lux = payload.lux;
    let luxCell = $(".panel-lux-" + box);
    luxCell.text(lux + "lx ");
    updateTime(payload.time);
  })


  document.querySelector(".panels-list").addEventListener("click", event => {
    channel.push("update_all", {})
  })


  channel.join()
    .receive("ok", resp => {
      // If we successfully join the channel, update the list of boxes
      console.log("Joined successfully", resp);
      channel.push("list", {});
    })
    .receive("error", resp => {
      console.log("Unable to join", resp);
    })

}
