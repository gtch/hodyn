import socket from "./socket"

let louvresTable = $(".louvres-areas");

if (louvresTable.length > 0) {
  // This page has a louvres table, so connect to the louvres channel and setup lourve functionality
  let channel = socket.channel("dynalite:louvre", {});

  let statusNames = {
    0: "Unknown",
    1: "Opening",
    2: "Open",
    3: "Closing",
    4: "Closed"
  };


  let updateTime = function(time) {
    $(".louvres-lastupdated").text("Last updated " + time);
  };

  let triggerLouvre = function(event) {
    let target = $(event.target);
    let louvre = target.data("louvre");
    let goal = target.data("goal");
    console.log("triggerLouvre", louvre, goal);
    if (goal === "open") {
      channel.push("open", { louvre: louvre });
    } else if (goal === "close") {
      channel.push("close", { louvre: louvre });
    } else {
      log.warn("Invalid goal " + goal + " for louvre " + louvre);
    }
  };

  channel.on("list", payload => {
    console.log("list", payload);
    louvresTable.empty();
    payload.louvres.forEach(louvreObject => {
      console.log("louvre", louvreObject);
      let name = louvreObject.name;
      let louvre = louvreObject.louvre;
      let louvreTd = $("<td>").html(name + " <span class=\"idnum\">(" + louvre + ")</span>");
      let statusTd = $("<td class='louvre-status'>").data("louvre", louvre).addClass("louvre-status-" + louvre);
      let openButton = $("<button class='louvre-open'>Open</button>").data("louvre", louvre).data("goal", "open").click(triggerLouvre);
      let closeButton = $("<button class='louvre-close'>Close</button>").data("louvre", louvre).data("goal", "close").click(triggerLouvre);
      let actionTd = $("<td class='louvre-actions'>").data("louvre", louvre).addClass("louvre-actions-" + louvre).append(openButton).append(closeButton);
      louvresTable.append($("<tr>").append(louvreTd, statusTd, actionTd));
    });
    channel.push("update_all", {});
  });


  channel.on("status", payload => {
    console.log("payload status", payload);
    let louvre = payload.louvre;
    let status = payload.status;
    let statusCell = $(".louvre-status-" + louvre);
    if (statusNames[status]) {
      statusCell.text(statusNames[status]);
    } else {
      statusCell.text("Unknown (" + status + ")");
    }

    updateTime(payload.time);
  });



  document.querySelector(".louvres-list").addEventListener("click", event => {
    channel.push("update_all", {})
  });



  channel.join()
    .receive("ok", resp => {
      // If we successfully join the channel, update the list of louvres
      console.log("Joined successfully", resp);
      channel.push("list", {});
    })
    .receive("error", resp => {
      console.log("Unable to join", resp);
    })


}
