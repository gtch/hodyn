defmodule Hodyn.PanelView do
  use Hodyn.Web, :view

  def render("show.json", %{panel: panel}) do
    %{ box: panel.box,
       temperature: panel.temperature,
       lux: panel.lux }
  end

end
