defmodule Hodyn.LouvresChannel do
  use Phoenix.Channel
  alias Hodyn.Gateway

  require Logger

  def join("dynalite:louvre", _message, socket) do
    {:ok, socket}
  end


  def handle_in("list", _, socket) do
    Logger.info("dynalite:louvre list received")
    broadcast! socket, "list", %{ louvres: [
      %{ louvre: 50, name: "Kitchen" },
      %{ louvre: 51, name: "Study" }
    ]}

    {:noreply, socket}
  end


  def handle_in("update_all", _, socket) do
    Logger.info("update_all received")
    Gateway.update_louvres()
    {:noreply, socket}
  end

  def handle_in("query_status", %{"louvre" => louvre}, socket) do
    Logger.info("query_status received for louvre #{louvre}")
    Gateway.get_louvre(louvre)
    {:noreply, socket}
  end

  def handle_in("query_goal", %{"louvre" => louvre}, socket) do
    Logger.info("query_goal received for louvre #{louvre}")
    Gateway.get_louvregoal(louvre)
    {:noreply, socket}
  end

  def handle_in("open", %{"louvre" => louvre}, socket) do
    Logger.info("open received for louvre #{louvre}")
    Gateway.open_louvre(louvre)
    {:noreply, socket}
  end

  def handle_in("close", %{"louvre" => louvre}, socket) do
    Logger.info("close received for louvre #{louvre}")
    Gateway.close_louvre(louvre)
    {:noreply, socket}
  end


end