defmodule Hodyn.LightsChannel do
  use Phoenix.Channel
  alias Hodyn.Gateway

  require Logger

  def join("dynalite:light", _message, socket) do
    {:ok, socket}
  end


  def handle_in("list", _, socket) do
    Logger.info("dynalite:light list received")
    broadcast! socket, "list", %{ areas: [
      %{ area: 26, name: "Path" },
      %{ area: 27, name: "Facade" },
      %{ area: 25, name: "Entry" },
      %{ area: 22, name: "Study" },
      %{ area: 20, name: "Courtyard" },
      %{ area: 17, name: "Dining" },
      %{ area: 16, name: "Kitchen" },
      %{ area: 15, name: "Pantry" },
      %{ area: 18, name: "Hall Downstairs" },
      %{ area: 8, name: "Bath Downstairs" },
      %{ area: 7, name: "Laundry" },
      %{ area: 5, name: "Store" },
      %{ area: 3, name: "Living" },
      %{ area: 9, name: "Garden" },
      %{ area: 32, name: "Stair" },
      %{ area: 36, name: "Hall Upstairs" },
      %{ area: 33, name: "Bath Upstairs" },
      %{ area: 37, name: "Bedroom 1" },
      %{ area: 30, name: "Bedroom 2" },
      %{ area: 38, name: "Roof Garden" }
    ]}

    {:noreply, socket}
  end


  def handle_in("update_all", _, socket) do
    Logger.info("update_all received")
    Gateway.update_lights()
    {:noreply, socket}
  end

  def handle_in("query_areastatus", %{"area" => area}, socket) do
    Logger.info("query_areastatus received for #{area}")
    Gateway.get_areastatus(area)
    {:noreply, socket}
  end

  def handle_in("area_preset", %{"area" => area, "preset" => preset}, socket) do
    Logger.info("area_preset #{preset} received for #{area}")
    Gateway.set_areapreset(area, preset)
    {:noreply, socket}
  end



end