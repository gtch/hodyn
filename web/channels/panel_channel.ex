defmodule Hodyn.PanelChannel do
  use Phoenix.Channel
  alias Hodyn.Gateway

  require Logger

  def join("dynalite:panel", _message, socket) do
    {:ok, socket}
  end


  def handle_in("list", _, socket) do
    Logger.info("dynalite:panel list received")
    broadcast! socket, "list", %{boxes: [
      %{ box: 78, name: "Entry" },
      %{ box: 80, name: "Study" },
      %{ box: 83, name: "Dining" },
      %{ box: 81, name: "Kitchen (Left)" },
      %{ box: 82, name: "Kitchen (Right)" },
      %{ box: 63, name: "Bathroom (Downstairs)" },
      %{ box: 62, name: "Living Room" },
      %{ box: 59, name: "Under Stairs" },
      %{ box: 61, name: "Stairs (Bottom)" },
      %{ box: 45, name: "Stairs (Top)" },
      %{ box: 60, name: "Bathroom (Upstairs)" },
      %{ box: 47, name: "Bedroom 1" },
      %{ box: 48, name: "Bedroom 1 (Amy)" },
      %{ box: 46, name: "Bedroom 1 (Charlie)" },
      %{ box: 44, name: "Bedroom 2" }
    ]}

    {:noreply, socket}
  end


  def handle_in("update_all", _, socket) do
    Logger.info("update_all received")
    Gateway.update_panels()
    {:noreply, socket}
  end

  def handle_in("query_temperature", %{"box" => box}, socket) do
    Logger.info("query_temperature received for #{box}")
    Gateway.get_temperature(box)
    {:noreply, socket}
  end

  def handle_in("identify", %{"box" => box}, socket) do
    Logger.info("identify received for #{box}")
    {:noreply, socket}
  end

  def handle_in("query_lux", %{"box" => box}, socket) do
    Logger.info("query_lux received for #{box}")
    Gateway.get_lux(box)
    {:noreply, socket}
  end


end
