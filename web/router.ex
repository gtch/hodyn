defmodule Hodyn.Router do
  use Hodyn.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Hodyn do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    get "/lights", PageController, :lights

    get "/louvres", PageController, :louvres

    get "/panels", PageController, :panels

  end

  # Other scopes may use custom stacks.
  scope "/api", Hodyn do
    pipe_through :api
  end
  
end
