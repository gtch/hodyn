# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :hodyn,
  ecto_repos: [Hodyn.Repo]

config :hodyn,
  setup_file: "./setup.json"

# Configures the endpoint
config :hodyn, Hodyn.Endpoint,
  url: [host: "localhost"],
  check_origin: ["//127.0.0.1", "//localhost"],
#  secret_key_base: "RTNBkVLJgI50y1QnL6ynesYcTYk6mc6cTO2yfYjCpscBBPz0aM5S3VHmVB3t7XwR",
  render_errors: [view: Hodyn.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Hodyn.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  level: :debug,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
