defmodule Hodyn do
  use Application

  require Logger

  def start(_type, _args) do
    import Supervisor.Spec

    {:ok, json} = load_setup_file()

    # Define workers and child supervisors to be supervised
    children = [
      # Start the endpoint when the application starts
      supervisor(Hodyn.Endpoint, []),
      # Dynalite Gateway
      supervisor(Hodyn.Gateway, [json]),
    ]

    opts = [strategy: :one_for_one, name: Hodyn.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Hodyn.Endpoint.config_change(changed, removed)
    :ok
  end


  def load_setup_file() do
    filename = Application.get_env(:hodyn, :setup_file)
    setup = with {:ok, body} <- File.read(filename),
                 {:ok, json} <- Poison.Parser.parse(body, keys: :atoms!),
             do: {:ok, json}
    Logger.debug("Loaded setup from #{filename}: #{inspect setup}")
    setup
  end

end
