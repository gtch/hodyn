defmodule Hodyn.Gateway do
  use Connection
  use Bitwise

  # Some inspirations:
  #  http://andrealeopardi.com/posts/handling-tcp-connections-in-elixir/
  #  http://www.fullyforged.com/2016/01/25/monitoring-an-external-tcp-service-in-elixir.html
  #  http://elixir-lang.org/getting-started/mix-otp/task-and-gen-tcp.html

  require Logger

  @update_interval 1000 * 60 * 5  # Update the temperatures and light levels every 5 minutes

  @retry_interval 1000 * 3        # If sending a message fails, retry in 3 seconds

  @send_interval 50               # Minimum of 20ms between each request into the Dynalite system


  defmodule State do
    defstruct host: "localhost",
              port: 50000,
              socket: nil,
              areas: [],
              boxes: [],
              louvres: %{},
              louvreAreas: [],
              queue: :queue.new(),
              lastSendTime: nil
  end

  def get_temperature(box) do
    Logger.info("Gateway -> get_temperature")
    Connection.call(:gateway, {:get_temperature, box})
  end

  def get_lux(box) do
    Logger.info("Gateway -> get_lux")
    Connection.call(:gateway, {:get_lux, box})
  end

  def get_areastatus(area) do
    Logger.info("Gateway -> get_areastatus")
    Connection.call(:gateway, {:get_areastatus, area})
  end

  def set_areapreset(area, preset) do
    Logger.info("Gateway -> set_areapreset")
    Connection.call(:gateway, {:set_areapreset, area, preset})
  end

  def get_louvre(louvre) do
    Logger.info("Gateway -> get_louvre")
    Connection.call(:gateway, {:get_louvre, louvre})
  end

  def get_louvregoal(louvre) do
    Logger.info("Gateway -> get_louvregoal")
    Connection.call(:gateway, {:get_louvregoal, louvre})
  end

  def open_louvre(louvre) do
    Logger.info("Gateway -> open_louvre")
    Connection.call(:gateway, {:open_louvre, louvre})
  end

  def close_louvre(louvre) do
    Logger.info("Gateway -> close_louvre")
    Connection.call(:gateway, {:close_louvre, louvre})
  end



  def update_lights() do
    Logger.info("Gateway -> update_lights")
    Connection.cast(:gateway, :update_lights)
  end

  def update_louvres() do
    Logger.info("Gateway -> update_louvres")
    Connection.cast(:gateway, :update_louvres)
  end

  def update_panels() do
    Logger.info("Gateway -> update_panels")
    Connection.cast(:gateway, :update_panels)
  end



  def start_link(opts) do
    Logger.info("Gateway -> start_link (opts=#{inspect opts})")
    state = opts_to_initial_state(opts)
    Connection.start_link(__MODULE__, state, name: :gateway)
  end

  def init(state) do
    Logger.info("Gateway -> init #{inspect state}")
    {:connect, nil, state}
  end

  def connect(_info, state) do
    Logger.info("Gateway -> connect")
    opts = [:binary, active: :true]
    case :gen_tcp.connect(state.host, state.port, opts) do
      {:ok, socket} ->
        {:ok, %{state | socket: socket}}
      {:error, reason} ->
        IO.puts "TCP connection error: #{inspect reason}"
        {:backoff, @retry_interval, state} # retry in fifteen seconds
    end
  end


  def command(pid, cmd) do
    Logger.debug("Gateway -> command  (pid=#{pid}, cmd=#{cmd})")
    Connection.call(pid, {:command, cmd})
  end


  defp schedule_update() do
    Logger.debug("Gateway -> schedule_update")
    Process.send_after(self(), :update_panels, @update_interval)
  end


  # Queries the light levels of the given Antumbra panels

  defp query_lux(box, state) do
    Logger.debug("Gateway -> query_lux (box=#{box})")
    getLux = << 92, 253, box, 183, 8, 0, 0 >>
    queue_dynet_command(getLux, state)
  end

  defp query_lux_multiple(boxes, state) when length(boxes) == 0 do
    Logger.debug("Gateway -> query_lux_multiple (boxes empty)")
    state
  end

  defp query_lux_multiple(boxes, state) do
    Logger.debug("Gateway -> query_lux_multiple (boxes=#{boxes})")
    state = query_lux(hd(boxes), state)
    query_lux_multiple(tl(boxes), state)
  end


  # Queries the temperature of the given Antumbra panels

  defp query_temperature(box, state) do
    Logger.debug("Gateway -> query_temperature (box=#{box})")
    getTemperature = << 92, 253, box, 183, 3, 0, 0 >>
    queue_dynet_command(getTemperature, state)
  end

  defp query_temperatures(boxes, state) when length(boxes) == 0 do
    Logger.debug("Gateway -> query_temperatures (boxes empty)")
    state
  end

  defp query_temperatures(boxes, state) do
    Logger.debug("Gateway -> query_temperatures (boxes=#{boxes})")
    state = query_temperature(hd(boxes), state)
    query_temperatures(tl(boxes), state)
  end


  # Queries the state of Dynalite areas

  defp query_areastatus(area, state) do
    Logger.debug("Gateway -> query_area (area=#{area})")
    getAreaStatus = << 28, area, 0, 99, 0, 0, 255 >>
    queue_dynet_command(getAreaStatus, state)
  end

  defp query_areastatuses(areas, state) when length(areas) == 0 do
    Logger.debug("Gateway -> query_areastatuses (areas empty)")
    state
  end

  defp query_areastatuses(areas, state) do
    Logger.debug("Gateway -> query_areastatuses (areas=#{areas})")
    state = query_areastatus(hd(areas), state)
    query_areastatuses(tl(areas), state)
  end


  # Sets a Dynalite area to a preset

  defp update_areapreset(area, preset, state) do
    Logger.debug("Gateway -> update_areapreset (area=#{area}, preset=#{preset})")
    fadeInTime = 50   # 50 x 20ms = 1 second
    setAreaPreset = case preset do
      1 -> << 28, area, fadeInTime, 0, 0, 0, 255 >>
      2 -> << 28, area, fadeInTime, 1, 0, 0, 255 >>
      3 -> << 28, area, fadeInTime, 2, 0, 0, 255 >>
      4 -> << 28, area, fadeInTime, 3, 0, 0, 255 >>
      5 -> << 28, area, fadeInTime, 10, 0, 0, 255 >>
      6 -> << 28, area, fadeInTime, 11, 0, 0, 255 >>
      7 -> << 28, area, fadeInTime, 12, 0, 0, 255 >>
      8 -> << 28, area, fadeInTime, 13, 0, 0, 255 >>
    end
    queue_dynet_command(setAreaPreset, state)
  end



  # Queries the state of a louvre

  defp query_louvre(louvre, state) do
    Logger.debug("Gateway -> query_louvre (louvre=#{louvre})")
    status = state.louvres[louvre].status
    Hodyn.Endpoint.broadcast!("dynalite:louvre", "status", %{ louvre: louvre, status: status, time: localTimeString() })
    state
  end

  defp query_louvregoal(louvre, state) do
    Logger.debug("Gateway -> query_louvregoal (louvre=#{louvre})")
    goal = state.louvres[louvre].goal
    Hodyn.Endpoint.broadcast!("dynalite:louvre", "goal", %{ louvre: louvre, goal: goal, time: localTimeString() })
    state
  end

  defp query_louvres(louvres, state) when length(louvres) == 0 do
    Logger.debug("Gateway -> query_louvres (louvres empty)")
    state
  end

  defp query_louvres(louvres, state) do
    Logger.debug("Gateway -> query_louvres (louvres=#{louvres})")
    louvre = hd(louvres)
    state = query_louvre(hd(louvres), state)
    query_louvres(tl(louvres), state)
  end


  # Opens or closes a louvre

  defp trigger_louvre(louvreArea, goal, state) do
    Logger.debug("Gateway -> trigger_louvre (louvre=#{louvreArea}, goal=#{goal})")

    louvre = state.louvres[louvreArea]
    sendTrigger = case { louvre.status, goal } do
      { :louvre_unknown, :louvre_open } ->
        true  # unknown state means we should try
      { :louvre_opening, :louvre_open } ->
        false # already opening
      { :louvre_open, :louvre_open } ->
        false # already open
      { :louvre_closing, :louvre_open } ->
        true  # stop closing
      { :louvre_closed, :louvre_open } ->
        true  # start opening
      { :louvre_unknown, :louvre_closed } ->
        true  # unknown state means we should try
      { :louvre_opening, :louvre_closed } ->
        true  # stop opening
      { :louvre_open, :louvre_closed } ->
        true  # start closing
      { :louvre_closing, :louvre_closed } ->
        false # already closing
      { :louvre_closed, :louvre_closed } ->
        false # already closed
      { status, goal } ->
        Logger.debug("No trigger_louvre handler defined for #{status} to #{goal}")
        false
    end

    louvre = %{louvre | goal: goal }
    louvres = Map.put(state.louvres, louvreArea, louvre)
    state = %{state | louvres: louvres}

    if (sendTrigger) do
      queue_dynet_command(<< 28, louvre.trigger, 0, 0, 0, 0, 255 >>, state)
    else
      state
    end
  end



  defp queue_dynet_command(baseCommand, state) do
    Logger.debug("Gateway -> queue_dynet_command (command=#{inspect baseCommand})")

    checksumIntermediate = Enum.reduce :binary.bin_to_list(baseCommand), 0, fn(num, acc) ->
      num + acc
    end
    checksum = (~~~(checksumIntermediate &&& 255) + 1) &&& 255
    command = baseCommand <> <<checksum>>

    Logger.debug("Queuing command  #{inspect command}")

    updatedQueue = :queue.in(command, state.queue)
    state = %{state | queue: updatedQueue}

    process_queue(state)
  end


  defp process_queue(state) do
    Logger.debug("Gateway -> process_queue")
    currentTimestamp = timestamp

    case { :queue.is_empty(state.queue), state.lastSendTime } do
      { true, _ } ->
        Logger.debug("No processing because queue is empty")
        state
      { false, lastSendTime } when lastSendTime == nil or lastSendTime + @send_interval < currentTimestamp ->
        Logger.debug("OK to process because last send time was #{lastSendTime} (current #{currentTimestamp})")
        case :queue.out(state.queue) do
          {{_value, item}, updatedQueue} ->
            process_dynet_command(item, %{state | queue: updatedQueue})
          _ ->
            Logger.info("process_queue found the queue to be empty")
            state
        end
      { false, lastSendTime } ->
        Logger.debug("Delaying processing because last send time was #{lastSendTime} (current #{currentTimestamp})")
        Process.send_after(self(), :process_queue, @send_interval)
        state
    end
  end

  defp process_dynet_command(command, state) do
    Logger.debug("Gateway -> process_dynet_command")
    Logger.info("Sending command #{inspect command}")

    case :gen_tcp.send(state.socket, command) do
      :ok ->
        Logger.debug("  send OK")
      {:error, reason} ->
        Logger.info("  send error #{inspect reason}")
    end

    %{state | lastSendTime: timestamp}
  end


  defp timestamp do
    :os.system_time(:milli_seconds)
  end

  defp localTimeString do
    {{year, month, day}, {hour, minute, second}} = :calendar.local_time()
    "#{hour}:#{String.rjust(Integer.to_string(minute),2,?0)}:#{String.rjust(Integer.to_string(second),2,?0)}"
  end


  def handle_call({:get_temperature, box}, from, state) do
    Logger.debug("Gateway -> handle_call temperature (box=#{box}, state=#{inspect state})")
    state = query_temperature(box, state)
    {:reply, state, state}
  end


  def handle_call({:get_lux, box}, from, state) do
    Logger.debug("Gateway -> handle_call lux (box=#{box}, state=#{inspect state})")
    state = query_lux(box, state)
    {:reply, state, state}
  end


  def handle_call({:get_areastatus, area}, from, state) do
    Logger.debug("Gateway -> handle_call areastatus (area=#{area}, state=#{inspect state})")
    state = query_areastatus(area, state)
    {:reply, state, state}
  end


  def handle_call({:set_areapreset, area, preset}, from, state) do
    Logger.debug("Gateway -> handle_call areapreset (area=#{area}, preset=#{preset} state=#{inspect state})")
    state = update_areapreset(area, preset, state)
    {:reply, state, state}
  end


  def handle_call({:get_louvre, louvre}, from, state) do
    Logger.debug("Gateway -> handle_call louvre (louvre=#{louvre}, state=#{inspect state})")
    state = query_louvre(louvre, state)
    {:reply, state, state}
  end


  def handle_call({:get_louvregoal, louvre}, from, state) do
    Logger.debug("Gateway -> handle_call louvregoal (louvre=#{louvre}, state=#{inspect state})")
    state = query_louvregoal(louvre, state)
    {:reply, state, state}
  end


  def handle_call({:open_louvre, louvre}, from, state) do
    Logger.debug("Gateway -> handle_call open_louvre (louvre=#{louvre}, state=#{inspect state})")
    state = trigger_louvre(louvre, :louvre_open, state)
    {:reply, state, state}
  end


  def handle_call({:close_louvre, louvre}, from, state) do
    Logger.debug("Gateway -> handle_call close_louvre (louvre=#{louvre}, state=#{inspect state})")
    state = trigger_louvre(louvre, :louvre_closed, state)
    {:reply, state, state}
  end


  def handle_cast(:update_panels, state) do
    Logger.debug("Gateway -> handle_cast update_panels (state=#{inspect state})")
    state = query_temperatures(state.boxes, state)
    state = query_lux_multiple(state.boxes, state)
    {:noreply, state}
  end


  def handle_info(:update_panels, state) do
    Logger.debug("Gateway -> handle_info update_panels (state=#{inspect state})")
    state = query_temperatures(state.boxes, state)
    state = query_lux_multiple(state.boxes, state)
    {:noreply, state}
  end


  def handle_cast(:update_lights, state) do
    Logger.debug("Gateway -> handle_cast update_lights (state=#{inspect state})")
    state = query_areastatuses(state.areas, state)
    {:noreply, state}
  end


  def handle_info(:update_lights, state) do
    Logger.debug("Gateway -> handle_info update_lights (state=#{inspect state})")
    state = query_areastatuses(state.areas, state)
#    schedule_update()
    {:noreply, state}
  end


  def handle_cast(:update_louvres, state) do
    Logger.debug("Gateway -> handle_cast update_louvres (state=#{inspect state})")
    state = query_louvres(Map.keys(state.louvres), state)
    {:noreply, state}
  end


  def handle_info(:update_louvres, state) do
    Logger.debug("Gateway -> handle_info update_louvres (state=#{inspect state})")
    state = query_louvres(state.louvreAreas, state)
    {:noreply, state}
  end


  def handle_info(:process_queue, state) do
    Logger.debug("Gateway -> handle_info process_queue")
    state = process_queue(state)
    {:noreply, state}
  end


  def handle_info({:tcp, socket, msg}, %{socket: socket} = state) do
    Logger.debug("Gateway -> handle_info tcp") # (state=#{inspect state})

    state = case byte_size(msg) do
      size when size == 8 ->
          handle_dynalite_command(msg, state)
      size when rem(size, 8) == 0 ->
          handle_dynalite_commands(msg, state)
      size ->
          Logger.warn("Unable to process Dynalite message of #{size} bytes")
          state
    end

    {:noreply, state}
  end


  defp handle_dynalite_commands(commands, state) do
    case byte_size(commands) do
      size when size < 8 ->
        Logger.warn("Cannot process Dynalite commands because message is #{size} bytes")
        state
      size ->
        # Take the first eight bytes and process those
        state = handle_dynalite_command(binary_part(commands, 0, 8), state)
        # Pass the remaining bytes back to this function to recursively process another eight bytes
        handle_dynalite_commands(binary_part(commands, 8, size - 8), state)
    end
  end


  defp handle_dynalite_command(command, state) do
    Logger.debug("Gateway -> handle_dynalite_command #{inspect command}") # (state=#{inspect state})

    case command do
      # Lights & Louvres
      << 28, area, presetZeroBased, 98, 0, 0, 255, checksum >> ->
        preset = presetZeroBased + 1
        handle_area_preset(area, preset, state)
      << 28, area, fadeLow, 0, fadeHi, 0, 255, checksum >>  ->
        handle_area_preset(area, 1, state)
      << 28, area, fadeLow, 1, fadeHi, 0, 255, checksum >>  ->
        handle_area_preset(area, 2, state)
      << 28, area, fadeLow, 2, fadeHi, 0, 255, checksum >>  ->
        handle_area_preset(area, 3, state)
      << 28, area, fadeLow, 3, fadeHi, 0, 255, checksum >>  ->
        handle_area_preset(area, 4, state)
      << 28, area, fadeLow, 10, fadeHi, 0, 255, checksum >>  ->
        handle_area_preset(area, 5, state)

      # Temperature (panel)
      << 92, 253, box, 184, 3, 0, tempQuarters, checksum >> ->
        temperature = tempQuarters / 4
        Logger.info("Received temperature #{temperature} for box #{box}")
        Hodyn.Endpoint.broadcast!("dynalite:panel", "temperature", %{ box: box, temperature: temperature, time: localTimeString() })
        state

      # Lux (ambient)
      << 92, 253, box, 184, 8, luxHigh, luxLow, checksum >> ->
        lux = luxHigh * 256 + luxLow
        Logger.info("Received lux #{lux} for box #{box}")
        Hodyn.Endpoint.broadcast!("dynalite:panel", "lux", %{ box: box, lux: lux, time: localTimeString() })
        state

      _ ->
        Logger.debug("Ignoring command: #{inspect command}")
        state
    end
  end

  defp handle_area_preset(area, preset, state) do
    Logger.info("Received preset #{preset} for area #{area}")
    if (area in state.louvreAreas) do
      # This is a louvre
      status = case preset do
        1 -> :louvre_opening
        2 -> :louvre_stopped
        3 -> :louvre_stopped
        4 -> :louvre_closing
      end
      handle_louvre_change(area, status, state)
    else
      # This is a light
      Hodyn.Endpoint.broadcast!("dynalite:light", "areastatus", %{ area: area, preset: preset, time: localTimeString() })
      state
    end
  end


  defp handle_louvre_change(louvreArea, status, state) do
    Logger.debug("Gateway -> handle_louvre_change (louvreArea=#{louvreArea}, status=#{status})")

    louvre = state.louvres[louvreArea]
    prevStatus = louvre.status
    newStatus = case { prevStatus, status } do
      { prevStatus, :louvre_opening } ->
        :louvre_opening
      { prevStatus, :louvre_closing } ->
        :louvre_closing
      { :louvre_opening, :louvre_stopped } ->
        :louvre_open
      { :louvre_closing, :louvre_stopped } ->
        :louvre_closed
      { prevStatus, :louvre_stopped } ->
        :louvre_unknown
      { prevStatus, newStatus } ->
        Logger.debug("No handler defined for #{prevStatus} to #{newStatus}")
        prevStatus
    end

    newGoal = case { prevStatus, status } do
      { :louvre_opening, :louvre_stopped } ->
        :louvre_open
      { :louvre_closing, :louvre_stopped } ->
        :louvre_closed
      { prevStatus, newStatus } ->
        #  keep the old goal until the window stops
        louvre.goal
    end

    Hodyn.Endpoint.broadcast!("dynalite:louvre", "status", %{ louvre: louvreArea, status: newStatus, time: localTimeString() })

    louvre = %{louvre | goal: newGoal }
    louvre = %{louvre | status: newStatus }
    louvres = Map.put(state.louvres, louvreArea, louvre)
    Map.put(state, :louvres, louvres)
  end



  defp opts_to_initial_state(opts) do
    gateway = opts.gateway
    host = Map.get(gateway, :host, "localhost") |> String.to_char_list
    port = Map.fetch!(gateway, :port)
    areas = Map.get(opts, :areas, []) |> Enum.map(fn(area) -> area.area end)
    boxes = Map.get(opts, :boxes, []) |> Enum.map(fn(box) -> box.box end)
    louvres = Map.get(opts, :louvres, %{})
    louvreMap = Map.new(louvres, fn(louvre) -> {louvre.relay, %{ name: louvre.name, trigger: louvre.trigger, goal: :louvre_unknown, status: :louvre_unknown }} end)
    louvreAreas = Enum.map(louvres, fn(louvre) -> louvre.relay end)
    %State{host: host, port: port, areas: areas, boxes: boxes, louvres: louvreMap, louvreAreas: louvreAreas}
  end


end
