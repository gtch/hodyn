defmodule Hodyn.Setup do
  @moduledoc false
  @derive [Poison.Encoder]

  defmodule SetupFile do
    defstruct [:gateway, :areas, :boxes, :louvres]
  end

  defmodule Gateway do
    defstruct [:host, :port]
  end

  defmodule Area do
    defstruct [:area, :name]
  end

end

